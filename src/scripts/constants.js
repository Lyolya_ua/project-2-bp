export const tarifs = Object.freeze({
    taxes: 0.2,
    water: 0.3,
    internet: 0.4,
    security: 0.5,
    exchange: 0.6,
});

export const dataTitles = {
    taxes: {
        title: 'Единый Налог',
        subTitle: 'Оплата единого налога'
    },
    water: {
        title: 'Холодная вода',
        subTitle: 'Оплата холодного водоснабжения'
    },
    internet: {
        title: 'Интернет',
        subTitle: 'Оплата интернета'
    },
    security: {
        title: 'Охрана дома',
        subTitle: 'Оплата охраны дома'
    },
    exchange: {
        title: 'Обмен валют',
        subTitle: 'Обмен валюты по курсу нац банка'
    }
};
