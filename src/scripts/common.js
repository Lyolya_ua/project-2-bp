import { companiesSideBar } from './index';
import { tarifs } from "./constants";

const meters = document.getElementById('meters');
const previous = document.getElementById('previous');
const current = document.getElementById('current');
const formSummaryList = document.querySelector('.form__summary-list');
const rightPayments = document.querySelector('.right__payments');
const transactionsList = document.querySelector('.transactions__list');
let balance = 100;

function getValidity(prev, cur) {
    if (typeof prev !== 'number' || isNaN(prev)) {
        previous.style.borderColor = 'red';
    } else if (typeof cur !== 'number' || isNaN(cur) || cur < prev) {
        current.style.borderColor = 'red';
    } else {
        previous.style.borderColor = 'red';
        current.style.borderColor = 'red';
    }
}

function getBill(obj) {
    const id = obj.id;
    const sum = ((obj.current - obj.previous) * tarifs[id]).toFixed(2);

    return +sum;
}

function getPaymentObject(obj) {
    const id = companiesSideBar.querySelector('.selected').getAttribute('data-id');
    const meterVal = meters.value;
    const prevVal = +previous.value;
    const curVal = +current.value;

    if (typeof prevVal === 'number' && !isNaN(prevVal) && typeof curVal === 'number' && !isNaN(curVal)) {
        if (prevVal > 0 && curVal > 0 && curVal > prevVal) {
            obj.id = id;
            obj.meterId = meterVal;
            obj.previous = prevVal;
            obj.current = curVal;
            obj.bill = getBill(obj);

            current.style.borderColor = '#dee1e5';
            previous.style.borderColor = '#dee1e5';

            return obj;
        } else {
            getValidity(prevVal, curVal);

            return false;
        }
    } else {
        getValidity(prevVal, curVal);

        return false;
    }
}

function getCheckBoxItem(obj, isCheck = true) {
    const label = rightPayments.querySelector(`label[data-id="${obj.id}"]`);
    const input = label.querySelector('input[type="checkbox"]');
    const text = label.querySelector('span').innerHTML;

    input.checked = isCheck ? true : false;

    return text;
}

function createPaymentItemHTML(obj) {
    const itemHTML = `<li class="list__item">
                <p><span class="list__item-label">${obj.meterId}</span>
                  <span class="price">$ <b>${obj.bill}</b></span>
                </p>
              </li>`;

    getCheckBoxItem(obj);

    return itemHTML;
}

function getPaymentItems(payments) {
    let listItems = formSummaryList.querySelectorAll('.list__item');
    const paymentItems = payments.map(item => {
        return createPaymentItemHTML(item);
    }).join('');

    listItems.forEach(item => {
        item.remove();
    });

    formSummaryList.insertAdjacentHTML('afterBegin', paymentItems);
    formSummaryList.insertAdjacentHTML('beforeend', getInvoice(payments));
}

function getInvoice(payments) {
    const bills = payments.map(item => item.bill);

    const total = bills.reduce(function (sum, current) {
        return sum + current;
    }, 0);

    const newTotal = total.toFixed(2);

    const totalItemHTML = `<li class="list__item list__total">
                <p><span class="list__item-label">Всего</span>
                  <span class="price">$ <b></b>${+newTotal}</span>
                </p>
              </li>`;

    return totalItemHTML;
}

function payTransactions(paymentsArray) {
    const newPaymentsArray = [];

    paymentsArray.forEach(item => {
        if(balance >= item.bill) {
            balance = (balance - item.bill).toFixed(2);
            balance = +balance;

            getTransactionHTML(item);
        } else {
            getTransactionHTML(item, true);
            newPaymentsArray.push(item);
        }
    });

    if(newPaymentsArray.length > 0) {
        const totalItem = formSummaryList.querySelector('.list__total');
        totalItem.remove();

        formSummaryList.insertAdjacentHTML('beforeend', getInvoice(newPaymentsArray));

        return newPaymentsArray;
    }
}

function getTransactionHTML(obj, isError = false) {
    const text = getCheckBoxItem(obj, isError);
    let item = '';

    if(isError) {
        item = `<li class="list__item list__item-error">${text}: ошибка транзакции</li>`;
    } else {
        const element = formSummaryList.querySelector('.list__item');
        item = `<li class="list__item">${text}: успешно оплачено</li>`;

        formSummaryList.removeChild(element);
    }

    transactionsList.insertAdjacentHTML('beforeend', item);
}

function resetObjectPayment(obj) {
    previous.value = '';
    current.value = '';

    for (let prop in obj) {
        delete obj[prop];
    }
}

function resetData(payments) {
    const transactionsItems = transactionsList.querySelectorAll('.list__item');
    const ckeckedItems = rightPayments.querySelectorAll('.right__payments-fields input');
    payments.length = 0;

    getPaymentItems(payments);

    transactionsItems.forEach( item => item.remove() );
    ckeckedItems.forEach( input => input.checked = false );
}

export { getPaymentObject, getPaymentItems, resetObjectPayment, resetData, payTransactions };
