import '../styles/index.scss';
import { dataTitles } from './constants';
import { getPaymentObject, getPaymentItems, resetObjectPayment, resetData, payTransactions } from './common';

export const totalItem = document.querySelector('.list__total .price b');
export const companiesSideBar = document.getElementById('companies');
const companies = companiesSideBar.querySelectorAll('.left__company');
const title = document.querySelector('.center__title');
const subTitle = document.querySelector('.center__desc');
const buttonWrapper = document.querySelector('.button-wrapper');
const save = buttonWrapper.querySelector('button[type="submit"]');
const submit = document.querySelector('.right button[type="submit"]');
const reset = buttonWrapper.querySelector('button[type="reset"]');

const payments = [];
const payment = {
    id: 'taxes',
};

(function () {
    if(localStorage.length > 0) {
        const initialPayment = JSON.parse(localStorage.getItem('payments')) || [];
        const lastIndex = initialPayment.length - 1;
        const attr = companiesSideBar.querySelector(`div[data-id="${initialPayment[lastIndex].id}"]`);

        attr.classList.add('selected');

        initialPayment.forEach( item => payments.push(item) );

        getPaymentItems(payments);

        localStorage.setItem('payments', JSON.stringify(payments));
    } else {
        const attr = companies[0].getAttribute('data-id');
        totalItem.innerHTML = '0';
        companies[0].classList.add('selected');

        for (let key in dataTitles) {
            if (key === attr) {
                title.innerHTML = dataTitles[key].title;
                subTitle.innerHTML = dataTitles[key].subTitle;
            }
        }
    }
})();

companies.forEach(item => {
    item.addEventListener('click', () => {
        const dataId = item.getAttribute('data-id');
        const element = companiesSideBar.querySelector('.selected');
        element.classList.remove('selected');
        item.classList.add('selected');

        for (let item in dataTitles) {
            if (item === dataId) {
                title.innerHTML = dataTitles[item].title;
                subTitle.innerHTML = dataTitles[item].subTitle;
            }
        }
    });
});

save.addEventListener('click', (event) => {
    event.preventDefault();
    const clone = {};

    if ( getPaymentObject(payment) ) {
        Object.assign(clone, payment);
        payments.push(clone);

        resetObjectPayment(payment);
        getPaymentItems(payments);

        localStorage.setItem('payments', JSON.stringify(payments));
    }

    //console.log(localStorage)
});

submit.addEventListener('click', (event) => {
    event.preventDefault();

    const newPayments = payTransactions(payments);

    if(newPayments) {
        payments.length = 0;

        newPayments.forEach(item => payments.push(item));

        localStorage.setItem('payments', JSON.stringify(payments));
    }

    //console.log(localStorage)
});

reset.addEventListener('click', (event) => {
    event.preventDefault();
    resetData(payments);

    localStorage.clear();

    //console.log(localStorage)
});

//console.log(localStorage)
